'''
Template Component main class.

'''

import logging
import os
import sys

import requests
from kbc.env_handler import KBCEnvHandler
from kbcstorage import tables

from kbc_scripts import kbcapi_scripts
from sql_generator import scd_sql_generators

# global constants'

# configuration variables
KEY_STORAGE_TOKEN = '#storage_token'
KEY_REGION = 'region'

KEY_SRC_TABLE = 'source_table'
KEY_DST_TABLE = 'destination_table'
KEY_SCD_PARAMETERS = 'scd_parameters'
KEY_OUTPUT_TR_BUCKET = 'output_tr_bucket'

KEY_SRC_TABLE_ID = 'src_table'
KEY_DST_TABLE_ID = 'dst_table_id'

KEY_MONITORED_PARAMS = 'monitored_params'
KEY_PRIMARY_KEY = 'primary_key'
KEY_KEEP_UNMONITORED = 'keep_unmonitored'

KEY_DST_BUCKET = 'tr_bucket'
KEY_BUCKET_OVERRIDE = 'tr_bucket_override'
KEY_TR_NAME = 'dst_tr_name'

KEY_USE_DATETIME = 'use_datetime'
KEY_SCD_TYPE = 'scd_type'
KEY_TIMEZONE = 'timezone'
KEY_KEEP_DELETED_ACTIVE = 'keep_del_active'
DELETED_FLAG = 'deleted_flag'

SUPPORTED_SCD_TYPES = ['scd2', 'scd4']

URL_REGION = {"connection.keboola.com": "US",
              "connection.eu-central-1.keboola.com": "EU"}

# #### Keep for debug
KEY_DEBUG = 'debug'
MANDATORY_PARS = [KEY_SRC_TABLE, KEY_DST_TABLE, KEY_SCD_PARAMETERS, KEY_OUTPUT_TR_BUCKET]
MANDATORY_IMAGE_PARS = []

APP_VERSION = '0.0.1'


class Component(KBCEnvHandler):

    def __init__(self, debug=False):
        KBCEnvHandler.__init__(self, MANDATORY_PARS)
        # override debug from config
        if self.cfg_params.get(KEY_DEBUG):
            debug = True

        log_level = logging.DEBUG if debug else logging.INFO
        if not os.getenv('KBC_LOGGER_PORT', None):
            # for debug purposes
            self.set_default_logger(log_level)
        else:
            self.set_gelf_logger(log_level)
        logging.info('Running version %s', APP_VERSION)
        logging.info('Loading configuration...')

        try:
            self.validate_config()
            self.validate_image_parameters(MANDATORY_IMAGE_PARS)
        except ValueError as e:
            logging.exception(e)
            exit(1)

        # init variables
        self.storage_token = self.get_storage_token()
        self.stack_id = os.environ["KBC_STACKID"]

        # fill defaults
        self.cfg_params[KEY_SCD_PARAMETERS][KEY_KEEP_UNMONITORED] = self.cfg_params.get(KEY_SCD_PARAMETERS, {}).get(
            KEY_KEEP_UNMONITORED, False)
        self.cfg_params[KEY_SCD_PARAMETERS][KEY_KEEP_DELETED_ACTIVE] = self.cfg_params.get(KEY_SCD_PARAMETERS, {}).get(
            KEY_KEEP_DELETED_ACTIVE, False)
        self.cfg_params[KEY_SCD_PARAMETERS][DELETED_FLAG] = self.cfg_params.get(KEY_SCD_PARAMETERS, {}).get(
            DELETED_FLAG, False)
        self.cfg_params[KEY_SCD_PARAMETERS][KEY_USE_DATETIME] = self.cfg_params.get(KEY_SCD_PARAMETERS, {}).get(
            KEY_USE_DATETIME, False)

    def run(self):
        '''
        Main execution code
        '''
        params = self.cfg_params  # noqa
        self.validate_cfg()
        scd_type = params[KEY_SCD_PARAMETERS][KEY_SCD_TYPE]
        src_table = params[KEY_SRC_TABLE][KEY_SRC_TABLE_ID].strip()
        table_details = ''
        try:
            table_details = self._get_storage_table_detail(src_table, self.storage_token, self.stack_id)
        except Exception as ex:
            logging.error(F'Failed to get the specified storage table "{src_table}"')
            logging.exception(ex)
            exit(1)
        logging.info("Validating configuration..")
        self.validate_output_objects()

        monitored_columns = self._parse_comma_sep_params(params[KEY_SRC_TABLE][KEY_MONITORED_PARAMS])
        primary_key_cols = self._parse_comma_sep_params(params[KEY_SRC_TABLE][KEY_PRIMARY_KEY])

        self.validate_columns(table_details, monitored_columns)
        self.validate_columns(table_details, primary_key_cols)

        conf_tr_bucket = params[KEY_OUTPUT_TR_BUCKET][KEY_DST_BUCKET]

        dst_table = None
        logging.info(F"Creating destination table {params[KEY_DST_TABLE][KEY_DST_TABLE_ID]}")
        try:
            dst_table = self._create_dst_table(params, [scd_sql_generators.COL_SNAP_PK],
                                               monitored_columns + primary_key_cols, scd_type)
        except Exception as ex:
            logging.error(F'Failed to create the destination table "{params[KEY_DST_TABLE][KEY_DST_TABLE_ID]}"')
            logging.exception(ex)

        dest_bucket = None

        if conf_tr_bucket[KEY_BUCKET_OVERRIDE]:
            logging.info(F'Creating trnasformation bucket {conf_tr_bucket["name"]}')
            dest_bucket = self.create_tr_bucket(conf_tr_bucket)
        else:
            dest_bucket = self.get_tr_bucket(conf_tr_bucket)

        if scd_type == 'scd2':
            logging.info("Generating SCD type 2 transformation SQL code.")
            sql_queries = scd_sql_generators.generate_scd2_code(monitored_columns, primary_key_cols,
                                                                params[KEY_SCD_PARAMETERS][KEY_KEEP_UNMONITORED],
                                                                params[KEY_SCD_PARAMETERS][KEY_KEEP_DELETED_ACTIVE],
                                                                params[KEY_SCD_PARAMETERS][DELETED_FLAG],
                                                                params[KEY_SCD_PARAMETERS][KEY_TIMEZONE],
                                                                params[KEY_SCD_PARAMETERS][KEY_USE_DATETIME])
            new_tr = self.create_transformation_row(sql_queries, table_details['id'], dst_table, dest_bucket['id'],
                                                    [scd_sql_generators.COL_SNAP_PK])
        elif scd_type == 'scd4':
            logging.info("Generating SCD type 4 transformation SQL code.")
            sql_queries = scd_sql_generators.generate_scd4_code(monitored_columns, primary_key_cols,
                                                                params[KEY_SCD_PARAMETERS][KEY_KEEP_UNMONITORED],
                                                                params[KEY_SCD_PARAMETERS][KEY_KEEP_DELETED_ACTIVE],
                                                                params[KEY_SCD_PARAMETERS][DELETED_FLAG],
                                                                params[KEY_SCD_PARAMETERS][KEY_TIMEZONE],
                                                                params[KEY_SCD_PARAMETERS][KEY_USE_DATETIME])
            new_tr = self.create_transformation_row(sql_queries, table_details['id'], dst_table, dest_bucket['id'],
                                                    [scd_sql_generators.COL_SNAP_PK])

        logging.info(
            f'Transformation created sucessfully! The new configuration named "{new_tr["name"]}" '
            f'is available in transformation bucket name: "{dest_bucket["name"]}" ID {dest_bucket["id"]}')

    def validate_cfg(self):
        validation_errors = ''
        try:
            self.validate_parameters(self.cfg_params[KEY_SRC_TABLE],
                                     [KEY_PRIMARY_KEY, KEY_MONITORED_PARAMS, KEY_SRC_TABLE_ID], 'Source table')
        except ValueError as e:
            validation_errors += str(e)

        try:
            self.validate_parameters(self.cfg_params[KEY_DST_TABLE], [KEY_DST_TABLE_ID], 'Destination table')
        except ValueError as e:
            validation_errors += str(e)

        try:
            self.validate_parameters(self.cfg_params[KEY_OUTPUT_TR_BUCKET], [KEY_TR_NAME], 'Transformation bucket')
        except ValueError as e:
            validation_errors += str(e)

        if not self.cfg_params[KEY_OUTPUT_TR_BUCKET][KEY_DST_BUCKET].get(KEY_BUCKET_OVERRIDE) \
                and not self.cfg_params[KEY_OUTPUT_TR_BUCKET][KEY_DST_BUCKET].get('id'):
            validation_errors += '\nYou must specify transformation bucket ID if you wish to reuse the bucket!'

        if self.cfg_params[KEY_OUTPUT_TR_BUCKET].get(KEY_BUCKET_OVERRIDE) \
                and not self.cfg_params[KEY_OUTPUT_TR_BUCKET].get('name'):
            validation_errors += '\nYou must specify transformation bucket name if you wish to create new one!'

        if validation_errors:
            raise ValueError(validation_errors)

    def create_transformation_row(self, queries, input_table_id, dst_table_id, dst_config_id, pkey_cols):
        row_cfg = self._build_tr_config(queries, dst_table_id, input_table_id,
                                        self.cfg_params[KEY_OUTPUT_TR_BUCKET][KEY_TR_NAME],
                                        pkey_cols)
        return kbcapi_scripts._create_config_row(self.storage_token, URL_REGION[self.stack_id], 'transformation',
                                                 dst_config_id,
                                                 self.cfg_params[KEY_OUTPUT_TR_BUCKET][KEY_TR_NAME], row_cfg)

    def _build_tr_config(self, queries, dst_table, input_table, name, pkey_columns):
        pkey_columns = [pk.lower() for pk in pkey_columns]
        cfg = {
            "output": [
                {
                    "primaryKey": pkey_columns,
                    "destination": dst_table,
                    "source": "final_snapshot",
                    "incremental": True
                }
            ],
            "queries": queries,
            "input": [
                {
                    "source": dst_table,
                    "destination": "curr_snapshot",
                    "whereColumn": "actual",
                    "whereValues": [
                        "1"
                    ],
                    "whereOperator": "eq",
                    "columns": []
                },
                {
                    "source": input_table,
                    "destination": "in_table"
                }
            ],
            "name": name,
            "phase": "1",
            "backend": "snowflake",
            "type": "simple",
            "disabled": False,
            "description": ""
        }
        return cfg

    def _get_storage_table_detail(self, table_id, token, stack_id):
        tablescl = tables.Tables('https://' + stack_id, token)
        return tablescl.detail(table_id)

        # if scd_type = 'scd2':

    def validate_columns(self, table_details, cols):
        missing_cols = list()
        for c in cols:
            if c not in table_details["columns"]:
                missing_cols.append(c)
        if missing_cols:
            raise ValueError(
                F'Some specified columns {missing_cols} were not found in specified table {table_details["id"]}. '
                F'Valid columns are {table_details["columns"]}')

    def _parse_comma_sep_params(self, param):
        cols = []
        if param:
            cols = [p.strip() for p in param.split(",")]
        return cols

    def create_tr_bucket(self, conf_tr_bucket):
        if conf_tr_bucket.get('id'):
            cfg = self.get_tr_bucket(conf_tr_bucket)
            if cfg:
                raise ValueError(F'Specified transformation bucket ID "{conf_tr_bucket.get("id")}" already exists. '
                                 F'Use different one or change override type to append.')

        return kbcapi_scripts._create_config(self.storage_token, URL_REGION[self.stack_id], 'transformation',
                                             conf_tr_bucket['name'], 'Generated by snapshotting tool', dict(),
                                             configurationId=conf_tr_bucket.get('id'))

    def get_tr_bucket(self, conf_tr_bucket):
        try:
            return kbcapi_scripts._get_config_detail(self.storage_token, URL_REGION[self.stack_id], 'transformation',
                                                     conf_tr_bucket.get('id'))
        except Exception as e:
            logging.error(F'Failed to get the specified transformation ID "{conf_tr_bucket.get("id")}"')
            raise e

    def validate_output_objects(self):
        err = []

        try:
            table_details = self._get_storage_table_detail(self.cfg_params[KEY_DST_TABLE][KEY_DST_TABLE_ID],
                                                           self.storage_token,
                                                           self.stack_id)
            if table_details:
                err.append(
                    F'Specified destination table already exists "{self.cfg_params[KEY_DST_TABLE][KEY_DST_TABLE_ID]}"')
            conf_tr_bucket = self.cfg_params[KEY_OUTPUT_TR_BUCKET][KEY_DST_BUCKET]
            if conf_tr_bucket[KEY_BUCKET_OVERRIDE]:
                cfg = self.get_tr_bucket(conf_tr_bucket)
                if cfg:
                    err.append(
                        F'Specified transformation bucket ID "{conf_tr_bucket.get("id")}" already exists. '
                        F'Use different one or change override type to append.')

        except requests.exceptions.HTTPError as ex:
            if ex.response.status_code == 404:
                pass
            else:
                logging.exception(ex)

        if err:
            raise ValueError(F'Output objects cannot be created: {err}')

    def _create_dst_table(self, params, pk_cols, monitored_cols, scd_type):
        table_id = params[KEY_DST_TABLE][KEY_DST_TABLE_ID].split('.')
        tablescl = tables.Tables('https://' + self.stack_id, self.storage_token)
        with open(os.path.join(self.data_path, 'dst.csv'), 'w+') as f:
            f.write(','.join(self._build_dst_header(pk_cols, monitored_cols, scd_type)))
        return tablescl.create(table_id[0] + '.' + table_id[1], table_id[2], file_path=f.name,
                               primary_key=[scd_sql_generators.COL_SNAP_PK])

    def _build_dst_header(self, pk_cols, monitored_cols, scd_type):
        header = [scd_sql_generators.COL_SNAP_PK]
        header.extend(pk_cols)
        header.extend(monitored_cols)
        if scd_type == 'scd2':
            header.extend([scd_sql_generators.COL_START_DATE, scd_sql_generators.COL_END_DATE])
        elif scd_type == 'scd4':
            header.extend([scd_sql_generators.COL_SNAP_DATE])

        header.extend([scd_sql_generators.COL_ACTUAL])
        if self.cfg_params[KEY_SCD_PARAMETERS][DELETED_FLAG]:
            header.append('is_deleted')
        header = list(set(header))
        return [h.lower() for h in header]


"""
        Main entrypoint
"""
if __name__ == "__main__":
    if len(sys.argv) > 1:
        debug_arg = sys.argv[1]
    else:
        debug_arg = False
    try:
        comp = Component(debug_arg)
        comp.run()
    except Exception as e:
        logging.exception(e)
        exit(1)
