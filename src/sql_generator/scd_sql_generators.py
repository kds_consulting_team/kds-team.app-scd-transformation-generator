import sqlparse

COL_SNAP_PK = 'snap_pk'
COL_START_DATE = 'start_date'
COL_END_DATE = 'end_date'
COL_ACTUAL = 'actual'
COL_IS_DELETED = 'is_deleted'

COL_SNAP_DATE = 'snapshot_date'


def generate_scd2_code(monitored_columns, primary_key_cols, keep_all_cols, keep_deleted_active, deleted_flag, timezone,
                       use_datetime=False):
    query_raw_template = """
        -- Auto-generated code using Snapshotting Generator. Modify as you please.

        SET CURR_DATE = (SELECT CONVERT_TIMEZONE('%(timezone)s', current_timestamp()))::DATE;
        SET CURR_TIMESTAMP = (SELECT CONVERT_TIMEZONE('%(timezone)s', current_timestamp())::TIMESTAMP_NTZ);
        SET CURR_DATE_TXT = (SELECT TO_CHAR($CURR_DATE, 'YYYY-MM-DD'));
        SET CURR_TIMESTAMP_TXT = (SELECT TO_CHAR($CURR_TIMESTAMP, 'YYYY-MM-DD HH:Mi:SS'));


        CREATE OR REPLACE TABLE changed_records_snapshot AS
        WITH
            diff_records AS (
                -- get records that have changed or have been added
                SELECT
                    %(input_table_cols_w_alias)s
                FROM "in_table" input
                MINUS
                SELECT
                    %(snap_table_cols_w_alias)s
                FROM "curr_snapshot" snap
                WHERE
                    "actual" = 1
            )
        SELECT
            %(input_table_cols)s

          , %(curr_date_value)s   AS "start_date"
          , '9999-12-31 00:00:00' AS "end_date"
          , 1                     AS "actual"
          , 0 AS "is_deleted"
        FROM diff_records;



        CREATE OR REPLACE TABLE deleted_records_snapshot AS
        SELECT
            %(snap_table_cols_w_alias)s
          , snap."start_date"   AS "start_date"
          , %(actual_deleted_timestamp)s AS "end_date"
          , %(actual_deleted_value)s                   AS "actual"
          , 1 AS "is_deleted"
        FROM
            "curr_snapshot" snap
                LEFT JOIN "in_table" input
                          ON %(snap_input_join_condition)s
        WHERE
            snap."actual" = 1 AND input.%(input_random_col)s IS NULL;

        CREATE OR REPLACE TABLE updated_snapshots AS
        SELECT
            %(snap_table_cols_w_alias)s

          , snap."start_date"
          , %(curr_date_value)s AS "end_date"
          , 0                   AS "actual"
          , 0 AS "is_deleted"
        FROM
            "curr_snapshot" snap
                JOIN changed_records_snapshot input
                     ON %(snap_input_join_condition)s
        WHERE
            snap."actual" = 1;

        -- final snapshot table
        CREATE OR REPLACE TABLE "final_snapshot" AS
        SELECT
        %(snap_primary_key_lower)s
          ,%(snap_table_cols)s
          ,%(snap_default_cols)s
        FROM deleted_records_snapshot
        UNION
        SELECT
        %(snap_primary_key_lower)s
          ,%(snap_table_cols)s
          ,%(snap_default_cols)s
        FROM updated_snapshots
        UNION
        SELECT
        %(snap_primary_key)s
          ,%(input_table_cols)s
          ,%(snap_default_cols)s
        FROM changed_records_snapshot
        ;
        """
    placeholder_values = dict()
    # input table columns pk + monitored
    input_table_cols_raw = list(set(primary_key_cols + monitored_columns))
    input_table_cols_w_alias_raw = ['input.' + '"' + i + '"' for i in input_table_cols_raw]

    placeholder_values['input_table_cols'] = ','.join(['"' + i + '"' for i in input_table_cols_raw])
    placeholder_values['input_table_cols_w_alias'] = ','.join(input_table_cols_w_alias_raw)

    # snap table cols normalized to lowercase
    snap_table_cols_raw = ['"' + i.lower() + '"' for i in input_table_cols_raw]
    snap_table_cols_w_alias_raw = ['snap.' + i for i in snap_table_cols_raw]

    placeholder_values['snap_table_cols'] = ','.join(snap_table_cols_raw)
    placeholder_values['snap_table_cols_w_alias'] = ','.join(snap_table_cols_w_alias_raw)

    # default cols
    snap_default_cols_raw = [COL_START_DATE, COL_END_DATE, COL_ACTUAL]
    if deleted_flag:
        snap_default_cols_raw.append(COL_IS_DELETED)
    placeholder_values['snap_default_cols'] = ','.join(['"' + i + '"' for i in snap_default_cols_raw])

    if use_datetime:
        curr_date_value = '$CURR_TIMESTAMP_TXT'
    else:
        curr_date_value = '$CURR_DATE_TXT'

    placeholder_values['curr_date_value'] = curr_date_value

    if not keep_deleted_active:
        placeholder_values['actual_deleted_value'] = 0
        placeholder_values['actual_deleted_timestamp'] = curr_date_value
    else:
        placeholder_values['actual_deleted_value'] = 1
        placeholder_values['actual_deleted_timestamp'] = "'9999-12-31 00:00:00'"

    placeholder_values['input_random_col'] = '"' + primary_key_cols[0] + '"'

    snap_input_join_condition = ['snap."' + pk.lower() + '"=' + 'input."' + pk + '"' for pk in primary_key_cols]
    placeholder_values['snap_input_join_condition'] = ' AND '.join(snap_input_join_condition)

    snap_primary_key = primary_key_cols + [COL_START_DATE]
    snap_primary_key_query_lower = ['"' + k.lower() + '"' for k in snap_primary_key]
    snap_primary_key_query = ['"' + k + '"' for k in snap_primary_key]
    placeholder_values['snap_primary_key_lower'] = "|| '|' ||".join(
        snap_primary_key_query_lower) + 'AS "' + COL_SNAP_PK + '"'
    placeholder_values['snap_primary_key'] = "|| '|' ||".join(snap_primary_key_query) + 'AS "' + COL_SNAP_PK + '"'
    placeholder_values['timezone'] = timezone

    res_sql = query_raw_template % placeholder_values
    statements = sqlparse.split(res_sql)
    formatted_sql_queries = list()
    for s in statements:
        formatted_sql_queries.append(sqlparse.format(s, reindent=True, keyword_case='upper'))
    return formatted_sql_queries


def generate_scd4_code(monitored_columns, primary_key_cols, keep_all_cols, keep_deleted_active, deleted_flag, timezone,
                       use_datetime=False):
    query_raw_template = """
        -- Auto-generated code using Snapshotting Generator. Modify as you please.


        SET CURR_DATE =  (SELECT CONVERT_TIMEZONE('Europe/Prague', current_timestamp()))::DATE;
        SET CURR_TIMESTAMP =  (SELECT CONVERT_TIMEZONE('Europe/Prague', current_timestamp())::TIMESTAMP_NTZ);
        SET CURR_DATE_TXT =  (SELECT TO_CHAR($CURR_DATE, 'YYYY-MM-DD'));
        SET CURR_TIMESTAMP_TXT =  (SELECT TO_CHAR($CURR_TIMESTAMP, 'YYYY-MM-DD HH:Mi:SS'));

        -- actual snapshot
        CREATE OR REPLACE TABLE records_snapshot AS
        SELECT %(input_table_cols)s
               , $CURR_TIMESTAMP_TXT AS "snapshot_date"
               , 1 AS "actual"
               , 0 AS "is_deleted"
        FROM "in_table";

        -- last snapshot rows to update actual flag
        CREATE OR REPLACE TABLE last_curr_records AS
        SELECT %(snap_table_cols)s
               , "snapshot_date"
               , 0 AS "actual"
               %(is_deleted_flag)s
        FROM "curr_snapshot"
        WHERE "actual" = 1 ;

        CREATE OR REPLACE TABLE deleted_records_snapshot AS
        SELECT %(snap_table_cols_w_alias)s
               , $CURR_TIMESTAMP_TXT AS "snapshot_date"
               , 1 AS "actual"
               , 1 AS "is_deleted"
        FROM "curr_snapshot" snap
        LEFT JOIN "in_table" INPUT ON snap."id"=input."ID"
        WHERE snap."actual" = 1
          AND input."ID" IS NULL;

        -- final snapshot table

        CREATE OR REPLACE TABLE "final_snapshot" AS
        SELECT %(snap_primary_key_lower)s
                  ,%(snap_table_cols)s
                  ,%(snap_default_cols)s
        FROM last_curr_records
        %(deleted_snap_query)s
        UNION
        SELECT     %(snap_primary_key)s
                  ,%(input_table_cols)s
                  ,%(snap_default_cols)s
        FROM records_snapshot ;
"""

    deleted_records_query = """
    UNION
    SELECT %(snap_primary_key_lower)s
              ,%(snap_table_cols)s
              ,%(snap_default_cols)s
    FROM deleted_records_snapshot
    """
    placeholder_values = dict()
    # input table columns pk + monitored
    input_table_cols_raw = list(set(primary_key_cols + monitored_columns))
    input_table_cols_w_alias_raw = ['input.' + '"' + i + '"' for i in input_table_cols_raw]

    placeholder_values['input_table_cols'] = ','.join(['"' + i + '"' for i in input_table_cols_raw])
    placeholder_values['input_table_cols_w_alias'] = ','.join(input_table_cols_w_alias_raw)

    # snap table cols normalized to lowercase
    snap_table_cols_raw = ['"' + i.lower() + '"' for i in input_table_cols_raw]
    snap_table_cols_w_alias_raw = ['snap.' + i for i in snap_table_cols_raw]

    placeholder_values['snap_table_cols'] = ','.join(snap_table_cols_raw)
    placeholder_values['snap_table_cols_w_alias'] = ','.join(snap_table_cols_w_alias_raw)

    # default cols
    snap_default_cols_raw = [COL_SNAP_DATE, COL_ACTUAL]
    placeholder_values['is_deleted_flag'] = ''
    if deleted_flag:
        snap_default_cols_raw.append(COL_IS_DELETED)
        placeholder_values['is_deleted_flag'] = ', "is_deleted"'
    placeholder_values['snap_default_cols'] = ','.join(['"' + i + '"' for i in snap_default_cols_raw])

    if use_datetime:
        curr_date_value = '$CURR_TIMESTAMP_TXT'
    else:
        curr_date_value = '$CURR_DATE_TXT'

    placeholder_values['curr_date_value'] = curr_date_value

    placeholder_values['input_random_col'] = '"' + primary_key_cols[0] + '"'

    snap_input_join_condition = ['snap."' + pk.lower() + '"=' + 'input."' + pk + '"' for pk in primary_key_cols]
    placeholder_values['snap_input_join_condition'] = ' AND '.join(snap_input_join_condition)

    snap_primary_key = primary_key_cols + [COL_SNAP_DATE]
    snap_primary_key_query_lower = ['"' + k.lower() + '"' for k in snap_primary_key]
    snap_primary_key_query = ['"' + k + '"' for k in snap_primary_key]
    placeholder_values['snap_primary_key_lower'] = "|| '|' ||".join(
        snap_primary_key_query_lower) + 'AS "' + COL_SNAP_PK + '"'
    placeholder_values['snap_primary_key'] = "|| '|' ||".join(snap_primary_key_query) + 'AS "' + COL_SNAP_PK + '"'
    placeholder_values['timezone'] = timezone

    # if deleted add union
    placeholder_values['deleted_snap_query'] = ''
    if keep_deleted_active:
        placeholder_values['deleted_snap_query'] = deleted_records_query % placeholder_values

    res_sql = query_raw_template % placeholder_values
    statements = sqlparse.split(res_sql)
    formatted_sql_queries = list()
    for s in statements:
        formatted_sql_queries.append(sqlparse.format(s, reindent=True, keyword_case='upper'))
    return formatted_sql_queries
