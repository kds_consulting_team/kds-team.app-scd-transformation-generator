# Snapshot (SCD) transformation generator

An application that generates transformation code and required initial tables to perform SCD type 2 and 4.

**Table of contents:**  
  
[TOC]

# Configuration


## Source table
The table you wish to snapshot.
   
- `ID`-  e.g. "out.c-test_snapshot.out_table" - full ID of the source table that you want to snapshot
- `Primary Key` - Comma separated list primary key columns of the source table
- `Monitored parameters` - Comma separated list of columns that will be "monitored". This means that these columns 
will be checked for changes and a new (SCD2) snapshot will be created whenever a change is recognized.

## Destination table
The result table where the snapshot will be stored.

- `ID` - Full Storage ID of the table e.g. "in.c-snapshots.test_snapshot_gen" - destination snapshot table

## Snapshot parameters
Configuration parameters affecting how the resulting snapshot is generated.

### SCD Type

Type of the snapshot generated snapshot / slowly changing dimension.

#### SCD Type 2
This is a most common type of slowly changing dimension. It produces a row whenever some of the monitored attributes is 
changed. For more information about type 2 [see here](https://en.wikipedia.org/wiki/Slowly_changing_dimension#Type_2:_add_new_row).

**Example**:

Consider we want to snapshot the input table with `Add is_deleted flag ` and `Include Time` parameters checked 
and monitoring the `VALUE` column.

The input table looks like this:

![input](docs/imgs/input_table.png)

The SCD Type 2 snapshot first run result will look like this:

![input](docs/imgs/first_result.png)

Now the column `VALUE` of the row ID 1 changes, the next run of the generated snapshot is going to look like this:

![input](docs/imgs/second_result.png)


#### SCD Type 4

This is the simplest type of snapshot, that tracks the state of the records each run (day), regardless the actual changes.

The `Monitored columns` parameter in this case works just a selector of which column should be included in the snapshot.


### Parameters

- **Add is_deleted flag** - If set, `is_deleted` column is included flagging records that had been removed 
from the source.
- **Include Time** - If set, the `snapshot_date` column will include time. Otherwise daily snapshots are captured.
- **Keep deleted active** - If set, deleted items will stay active i.e. `actual=1`
- **Timezone** - important parameter specifying timezone you want to snapshot in. If specified incorrectly, the current 
date generated may be different from what you expect.


## Result Transformation

The application generates Snowflake transformation along with all required input mappings and also creates 
the destination table that is included in the input mapping for feedback.

The code will vary based on the `Snapshot parameters`. The resulting transformation is ready to run / orchestrate 
and the snapshots will be created incrementally as you trigger them.

### Parameters:

- ** Transformation name** - [REQ] The resulting name of the transformation (row)
- ** Transformation bucket** [REQ] Parameters of the resulting transformation bucket.
    - `name` - The resulting bucket name
    - `ID` - The resulting bucket ID. Required only if the `Create new` parameter is set to `No`
    - `Create new` - When set to yes, a new bucket is created. When set to no, the `ID` parameter is required and the 
    resulting transformation will be injected into an existing bucket.
    
Below is example of generated input mapping:

![input](docs/imgs/generated_mapping.png)


# Output structure

The result table looks like this:

![input](docs/imgs/second_result.png)

## Columns
- **snap_pk** - primary key formed from Source table primary key columns and `snapshot_date`
- **source table cols** - any columns selected by `Monitored columns` parameter.
- **start_date** - Date from when the record is active
- **end_date** - Date until when the record were active. If still active the 9999 date is added.
- **snapshot_date** - Snapshot date, available for SCD type 4, instead of `start_date` and `from_date`__
- **actual** - Flag whether it is the latest actual record.
- **is_deleted** - If setup, this flags that the item was deleted from the monitored source table
    


# Development
 
This example contains runnable container with simple unittest. For local testing it is useful to include `data` folder in the root
and use docker-compose commands to run the container or execute tests. 

If required, change local data folder (the `CUSTOM_FOLDER` placeholder) path to your custom path:
```yaml
    volumes:
      - ./:/code
      - ./CUSTOM_FOLDER:/data
```

Clone this repository, init the workspace and run the component with following command:

```
git clone https://bitbucket.org:kds_consulting_team/kbc-python-template.git my-new-component
cd my-new-component
docker-compose build
docker-compose run --rm dev
```

Run the test suite and lint check using this command:

```
docker-compose run --rm test
```

# Integration

For information about deployment and integration with KBC, please refer to the [deployment section of developers documentation](https://developers.keboola.com/extend/component/deployment/) 