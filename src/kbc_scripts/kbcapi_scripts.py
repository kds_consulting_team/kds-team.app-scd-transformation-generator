import json
import os
import urllib

import requests
from kbcstorage.base import Endpoint
from kbcstorage.buckets import Buckets
from kbcstorage.tables import Tables

# uncomment in sandbox
# import subprocess
# import sys
# subprocess.call([sys.executable, '-m', 'pip', 'install', 'git+https://github.com/keboola/sapi-python-client.git'])

URL_SUFFIXES = {"US": ".keboola.com",
                "EU": ".eu-central-1.keboola.com"}

"""
Various Adhoc scripts for KBC api manipulations.

"""


def run_config(component_id, config_id, token, region='US'):
    values = {
        "config": config_id
    }

    headers = {
        'Content-Type': 'application/json',
        'X-StorageApi-Token': token
    }
    response = requests.post('https://syrup' + URL_SUFFIXES[region] + '/docker/' + component_id + '/run',
                             data=json.dumps(values),
                             headers=headers)

    try:
        response.raise_for_status()
    except requests.HTTPError as e:
        raise e
    else:
        return response.json()


def get_job_status(token, url):
    headers = {
        'Content-Type': 'application/json',
        'X-StorageApi-Token': token
    }
    response = requests.get(url, headers=headers)
    try:
        response.raise_for_status()
    except requests.HTTPError as e:
        raise e
    else:
        return response.json()


def _get_config_detail(token, region, component_id, config_id):
    """

    :param region: 'US' or 'EU'
    """
    cl = Endpoint('https://connection' + URL_SUFFIXES[region], 'components', token)
    url = '{}/{}/configs/{}'.format(cl.base_url, component_id, config_id)
    return cl._get(url)


def _get_config_rows(token, region, component_id, config_id):
    """
    Retrieves component's configuration detail.

    Args:
        component_id (str or int): The id of the component.
        config_id (int): The id of configuration
        region: 'US' or 'EU'
    Raises:
        requests.HTTPError: If the API request fails.
    """
    cl = Endpoint('https://connection' + URL_SUFFIXES[region], 'components', token)
    url = '{}/{}/configs/{}/rows'.format(cl.base_url, component_id, config_id)

    return cl._get(url)


def _create_config(token, region, component_id, name, description, configuration, configurationId=None, state=None,
                   changeDescription='', **kwargs):
    """
    Create a new table from CSV file.

    Args:
        component_id (str):
        name (str): The new table name (only alphanumeric and underscores)
        configuration (dict): configuration JSON; the maximum allowed size is 4MB
        state (dict): configuration JSON; the maximum allowed size is 4MB
        changeDescription (str): Escape character used in the CSV file.
        region: 'US' or 'EU'

    Returns:
        table_id (str): Id of the created table.

    Raises:
        requests.HTTPError: If the API request fails.
    """
    cl = Endpoint('https://connection' + URL_SUFFIXES[region], 'components', token)
    url = '{}/{}/configs'.format(cl.base_url, component_id)
    parameters = {}
    if configurationId:
        parameters['configurationId'] = configurationId
    parameters['configuration'] = json.dumps(configuration)
    parameters['name'] = name
    parameters['description'] = description
    parameters['changeDescription'] = changeDescription
    if state:
        parameters['state'] = json.dumps(state)
    header = {'Content-Type': 'application/x-www-form-urlencoded'}
    data = urllib.parse.urlencode(parameters)
    return cl._post(url, data=data, headers=header)


def _create_config_row(token, region, component_id, configuration_id, name, configuration,
                       description='', rowId=None, state=None, changeDescription='', isDisabled=False, **kwargs):
    """
    Create a new table from CSV file.

    Args:
        component_id (str):
        name (str): The new table name (only alphanumeric and underscores)
        configuration (dict): configuration JSON; the maximum allowed size is 4MB
        state (dict): configuration JSON; the maximum allowed size is 4MB
        changeDescription (str): Escape character used in the CSV file.
        region: 'US' or 'EU'

    Returns:
        table_id (str): Id of the created table.

    Raises:
        requests.HTTPError: If the API request fails.
    """
    cl = Endpoint('https://connection' + URL_SUFFIXES[region], 'components', token)
    url = '{}/{}/configs/{}/rows'.format(cl.base_url, component_id, configuration_id)
    parameters = {}
    # convert objects to string
    parameters['configuration'] = json.dumps(configuration)
    parameters['name'] = name
    parameters['description'] = description
    if rowId:
        parameters['rowId'] = rowId
    parameters['changeDescription'] = changeDescription
    parameters['isDisabled'] = isDisabled
    if state:
        parameters['state'] = json.dumps(state)

    header = {'Content-Type': 'application/x-www-form-urlencoded'}
    data = urllib.parse.urlencode(parameters)
    return cl._post(url, data=data, headers=header)


def clone_orchestration(src_token, dest_token, src_region, dst_region, orch_id):
    """
    Clones orchestration. Note that all component configs that are part of the tasks need to be migrated first using
    the migrate_config function. Otherwise it will fail.
    :param src_token:
    :param orch_id:
    :param dest_token:
    :param region:
    :return:
    """
    src_config = _get_config_detail(src_token, src_region, 'orchestrator', orch_id)
    return _create_orchestration(dest_token, dst_region, src_config['name'], src_config['configuration']['tasks'])


def _create_orchestration(token, region, name, tasks):
    values = {
        "name": name,
        "tasks": tasks
    }

    headers = {
        'Content-Type': 'application/json',
        'X-StorageApi-Token': token
    }
    response = requests.post('https://syrup' + URL_SUFFIXES[region] + '/orchestrator/orchestrations',
                             data=json.dumps(values),
                             headers=headers)

    try:
        response.raise_for_status()
    except requests.HTTPError as e:
        raise e
    else:
        return response.json()


def run_orchestration(orch_id, token, region='US'):
    headers = {
        'Content-Type': 'application/json',
        'X-StorageApi-Token': token
    }
    response = requests.post(
        'https://syrup' + URL_SUFFIXES[region] + '/orchestrator/orchestrations/' + str(orch_id) + '/jobs',
        headers=headers)

    try:
        response.raise_for_status()
    except requests.HTTPError as e:
        raise e
    else:
        return response.json()


def get_orchestrations(token, region='US'):
    syrup_cl = Endpoint('https://syrup' + URL_SUFFIXES[region], 'orchestrator', token)

    url = syrup_cl.root_url + '/orchestrator/orchestrations'
    res = syrup_cl._get(url)
    return res


def _download_table(table, client: Tables, out_file):
    print('Downloading table %s into %s from source project', table['id'], out_file)
    res_path = client.export_to_file(table['id'], out_file, is_gzip=True, changed_until='')

    return res_path


PAR_WORKDIRPATH = os.path.dirname(os.path.join(os.path.abspath('')))


def transfer_storage_bucket(from_token, to_token, src_bucket_id, region_from='EU', region_to='EU', dest_bucket_id=None,
                            tmp_folder=os.path.join(PAR_WORKDIRPATH, 'data')):
    storage_api_url_from = 'https://connection' + URL_SUFFIXES[region_from]
    storage_api_url_to = 'https://connection' + URL_SUFFIXES[region_to]
    from_tables = Tables(storage_api_url_from, from_token)
    from_buckets = Buckets(storage_api_url_from, from_token)
    to_tables = Tables(storage_api_url_to, to_token)
    to_buckets = Buckets(storage_api_url_to, to_token)
    print('Getting tables from bucket %s', src_bucket_id)
    tables = from_buckets.list_tables(src_bucket_id)

    if dest_bucket_id:
        new_bucket_id = dest_bucket_id
    else:
        new_bucket_id = src_bucket_id

    bucket_exists = (new_bucket_id in [b['id'] for b in to_buckets.list()])

    for tb in tables:
        tb['new_id'] = tb['id'].replace(src_bucket_id, new_bucket_id)
        tb['new_bucket_id'] = new_bucket_id

        if bucket_exists and tb['new_id'] in [b['id'] for b in to_buckets.list_tables(new_bucket_id)]:
            print('Table %s already exists in destination bucket, skipping..', tb['new_id'])
            continue

        local_path = _download_table(tb, from_tables, tmp_folder)

        b_split = tb['new_bucket_id'].split('.')

        if not bucket_exists:
            print('Creating new bucket %s in destination project', tb['new_bucket_id'])
            to_buckets.create(b_split[1].replace('c-', ''), b_split[0])
            bucket_exists = True

        print('Creating table %s in the destination project', tb['id'])

        to_tables.create(tb['new_bucket_id'], tb['name'], local_path,
                         primary_key=tb['primaryKey'])
        # , compress=True)

        print('Deleting temp file')
        os.remove(local_path)
        # os.remove(local_path + '.gz')

    print('Finished.')


def migrate_configs(src_token, dst_token, src_config_id, component_id, src_region='EU', dst_region='EU',
                    use_src_id=False):
    """
    Super simple method, getting all table config objects and updating/creating them in the destination configuration.
    Includes all attributes, even the ones that are not updateble => API service will ignore them.

    :par use_src_id: If true the src config id will be used in the destination

    """
    src_config = _get_config_detail(src_token, src_region, component_id, src_config_id)
    src_config_rows = _get_config_rows(src_token, src_region, component_id, src_config_id)

    dst_config = src_config.copy()
    # add component id
    dst_config['component_id'] = component_id

    if use_src_id:
        dst_config['configurationId'] = src_config['id']

    # add token and region to use wrapping
    dst_config['token'] = dst_token
    dst_config['region'] = dst_region

    print('Transfering config..')
    new_cfg = _create_config(**dst_config)

    print('Transfering config rows')
    for row in src_config_rows:
        row['component_id'] = component_id
        row['configuration_id'] = new_cfg['id']
        row['configuration'].pop('id', {})
        row['configuration'].pop('rowId', {})
        row.pop('rowId', {})

        # add token and region to use wrapping
        row['token'] = dst_token
        row['region'] = dst_region

        _create_config_row(**row)
